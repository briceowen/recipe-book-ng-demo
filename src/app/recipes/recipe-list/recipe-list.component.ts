import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe(
      'Recipe 1',
      'Recipe Description',
      'https://i2.wp.com/wellplated.com/wp-content/uploads/2019/03/Slow-Cooker-Honey-Garlic-Chicken-and-Veggies-600x900.jpg'
    ),
    new Recipe(
      'Recipe 1',
      'Recipe Description',
      'https://i2.wp.com/wellplated.com/wp-content/uploads/2019/03/Slow-Cooker-Honey-Garlic-Chicken-and-Veggies-600x900.jpg'
    )
  ];

  constructor() {}

  ngOnInit() {}
}
